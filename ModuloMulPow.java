/**
 * Created by Przemek on 21.01.2017.
 */
public class ModuloMulPow implements ModuloMulPowInterface {
    public long mull(long a, long b, long c) {
        long w,m;
        for(w=0,m=1;m!=0;m<<=1) {
            if((b & m)!=0) {
                w = (w + a) % c;
            }
            a=(a<<1)%c;
        }
        return w;
    }
    public long pow(long a, long b, long c) {
        long w,p,m;
        for(m=w=1,p=a; m!=0 ;m<<=1){
            if((b&m)!=0) {
                w = mull(w, p, c);
            }
            p=mull(p,p,c);
        }
        return w;
    }
}
