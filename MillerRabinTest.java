import java.util.*;

/**
 * Created by Przemek on 21.01.2017.
 */
public class MillerRabinTest extends ModuloMulPow implements PrimeNumberTest  {
    public MillerRabinTest() {
        numbers = new TreeSet<Long>();
    }

    public Set<Long> getNumbers() {
        return numbers;
    }

    public void setNumbers(Set<Long> numbers) {
        this.numbers = numbers;
    }

    private Set<Long> numbers;

    public long getN() {
        return n;
    }

    public void setN(long n) {
        this.n = n;
    }

    private long n;

    private void fillnumber(){
        numbers.clear();
        Random generator = new Random();
        long q;
        if(n>20)
            q = 10;
        else
            q = (n - 2) / 2;
        for(;numbers.size()<q;) {
            Long a = new Long(generator.nextInt((int) n - 2) + 2);
            if(numbers.contains(a)==false){
                numbers.add(new Long(a));
            }
        }
    }

    public boolean check(long a) throws NumberInternalException{
        if(a<=2){
            throw new NumberInternalException();
        }
        n=a;
        fillnumber();
        System.out.println(numbers);
        boolean check=true;
        long q,d;
        for(q=a-1,d=0;q%2==0;d++){
            q=q/2;
        }
        for(Long x:numbers){
            long p=pow(x,q,n);
            if(p==1 || p==x-1) {
            check= true;
            } else{
                for(long j=0;j<d && p!=n-1;j++) {
                    p=mull(p,p,n);
                    if(p==1) {
                    check= false;
                    }
                }
                if(p!=n-1) {
                    check= false;
                }
            }
            if(!check){
                return false;
            }
        }

        return true;
    }
}
