/**
 * Created by Przemek on 21.01.2017.
 */
public interface PrimeNumberTest {
    boolean check(long a) throws NumberInternalException;
}
