/**
 * Created by Przemek on 21.01.2017.
 */
public interface ModuloPowInterface {
   long pow(long a, long b, long c);
}
