import java.util.Scanner;
import java.util.Set;

/**
 * Created by Przemek on 21.01.2017.
 */



public class Main {
    public static void main(String[] args) {

        PrimeNumberTest primeNumberTest = new MillerRabinTest();
        Scanner in=new Scanner(System.in);
        String s;

        while (true){
            System.out.println("Please, give a number:");
            long numb = in.nextLong();
            try {
                boolean check = primeNumberTest.check(numb);
                if (check == true) {
                    System.out.println("It's prime number");
                } else {
                    System.out.println("it's not a prime number");
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            System.out.println("Do you want to try again? Y/N");
            s=in.next();
            if(s.equals("N")){
                break;
            }
        }

    }

}
