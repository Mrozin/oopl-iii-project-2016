/**
 * Created by Przemek on 22.01.2017.
 */
public class NumberInternalException extends Exception {
    @Override
    public String getMessage(){
        return "Number must be higher than 2";
    }

}
